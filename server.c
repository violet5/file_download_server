#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <time.h>

#include <dirent.h>

#include <libsocket/libinetsocket.h>

#include "md5.h"

long get_filesize(char *filepath);

void clear_buffer(char *buf, int length);

char *directory = "/home/vagrant/13-file_download_server/downloads";

char *help_message = "hello, this is "
    "a help message\n";

char *unrecognized_command = "unrecognized command\n";
char *goodbye_message = "goodbye!\n";
char *no_such_file = "-ERR No such file\n";

struct file_s {
    char filename[100];
    char filepath[250];
    short accessed_hash;
    long filesize;
    char list;
};

float rand_float();
void list_files(int sfd, struct file_s *files, int filecount);
void send_file(int sfd, char *line, struct file_s *files, int filecount);
void send_hash(int sfd, char *line, struct file_s *files, int filecount);
void send_size(int sfd, char *line, struct file_s *files, int filecount);
void send_hello(int sfd, char *line);
struct file_s *get_file_list(int *filecount);



int main(int argc, char **argv) {
    if (argc != 2) {
        printf("usage: %s port\n", argv[0]);
    }

    srand(time(NULL));

    int filecount;
    // printf("getting file list\n");
    struct file_s *files = get_file_list(&filecount);
    // printf("printing file list\n");
    // for (int i = 0 ; i < filecount ; i++) {
    //     printf(
    //         "filename: %s; filepath: %s; accessed_hash: %d; filesize: %ld\n",
    //         files[i].filename, files[i].filepath, files[i].accessed_hash, files[i].filesize
    //     );
    // }

    int socknum = create_inet_server_socket(
        // "192.168.1.111",
        // "10.0.2.15",
        "127.0.0.1",
        argv[1],
        LIBSOCKET_TCP,
        LIBSOCKET_IPv4,
        0
    );
    if (socknum == -1) {
        fprintf(stderr, "Couldn't connect\n");
        exit(1);
    }
    // printf("socknum: %d\n", socknum);
    char src_host[100];
    size_t src_host_len = 0;
    char src_service[6];
    size_t src_service_len = 0;
    int sfd = accept_inet_stream_socket(
        socknum,
        src_host,
        src_host_len,
        src_service,
        src_service_len,
        // flags
        0,
        // accept_flags
        0
    );
    if (sfd == -1) {
        fprintf(stderr, "Failed to get stream file descriptor from connecting client\n");
        exit(2);
    }

    int line_length = 100;
    char line[line_length];
    clear_buffer(line, line_length);

    char *greeting = "+OK Greetings\n";
    write(sfd, greeting, strlen(greeting));


    while (1) {
        // printf("reading command from client\n");
        read(sfd, line, line_length);
        // printf("read command line '%s' from client\n", line);

        // chop off newline
        line[strlen(line)-1] = '\0';
        // printf("got: '%s'\n", line);

        if (strncmp(line, "HELP", 4)==0) {
            write(sfd, help_message, strlen(help_message));
        }
        else if (strncmp(line, "LIST", 4)==0) {
            list_files(sfd, files, filecount);
        }
        else if (strncmp(line, "GET", 3)==0) {
            send_file(sfd, line, files, filecount);
        }
        else if (strncmp(line, "HASH", 4)==0) {
            send_hash(sfd, line, files, filecount);
        }
        else if (strncmp(line, "SIZE", 4)==0) {
            send_size(sfd, line, files, filecount);
        }
        else if (strncmp(line, "HELO", 4)==0) {
            send_hello(sfd, line);
        }
        else if (strncmp(line, "QUIT", 4)==0) {
            write(sfd, goodbye_message, strlen(goodbye_message));
            break;
        }
        else {
            write(sfd, unrecognized_command, strlen(unrecognized_command));
        }

        // switch (line) {
        //     case "help":
        //         break;
        //     default:
        //         break;
        // }


        // write(sfd, line, strlen(line));

        clear_buffer(line, line_length);
    }

    // TODO one of the sides may not have consumed all the data.
    // the server might not have consumed all the data, or may have not sent a necessary newline at the end.
    // 

    // flush(sfd);
    // close(sfd);
    // flush(socknum);
    // close(socknum);
    // int success = shutdown_inet_stream_socket(socknum, LIBSOCKET_READ|LIBSOCKET_WRITE) == 0;
    shutdown_inet_stream_socket(socknum, LIBSOCKET_READ|LIBSOCKET_WRITE);
    // printf("success shutdown_inet_stream_socket: %d\n", success);
    // success = destroy_inet_socket(socknum) == 0;
    // printf("success destroy_inet_socket: %d\n", success);

    free(files);
}

void clear_buffer(char *buf, int length) {
    for (int i = 0 ; i < length ; i++) {
        buf[i] = 0;
    }
}

void list_files(int sfd, struct file_s *files, int filecount) {
    char line[250];
    for (int i = 0 ; i < filecount ; i++) {
        if (!files[i].list)
            continue;
        sprintf(line, "%ld %s\n", files[i].filesize, files[i].filename);
        write(sfd, line, strlen(line));
    }
    write(sfd, ".\n", 2);
    // printf("finished printing directory stuffs\n");
}

struct file_s *get_file_list(int *filecount) {
    *filecount = 0;

    DIR *dir = opendir(directory);
    if (dir == NULL) {
        printf("Could not open directory '%s'\n", directory);
        return NULL;
    }
    struct dirent *de;
    char filepath[257];
    while ((de = readdir(dir)) != NULL) {
        if (strncmp(de->d_name, ".", 1)==0 || strncmp(de->d_name, "..", 2)==0)
            continue;
        *filecount += 1;
    }
    struct file_s *files = malloc((*filecount)*sizeof(struct file_s));
    int i = 0;
    dir = opendir(directory);
    if (dir == NULL) {
        printf("Could not open directory '%s'\n", directory);
        return NULL;
    }
    while ((de = readdir(dir)) != NULL) {
        if (strncmp(de->d_name, ".", 1)==0 || strncmp(de->d_name, "..", 2)==0)
            continue;
        sprintf(filepath, "%s/%s", directory, de->d_name);
        // sprintf(line, "%ld %s\n", get_filesize(filepath), de->d_name);
        strncpy(files[i].filename, de->d_name, 100);
        strncpy(files[i].filepath, filepath, 250);
        files[i].accessed_hash = 0;
        files[i].filesize = get_filesize(filepath);
        files[i].list = rand_float()>0.15;
        i++;
    }
    return files;
}

long get_filesize(char *filepath) {
    struct stat stat_f;
    lstat(filepath, &stat_f);
    return stat_f.st_size;
}

float rand_float() {
    return (rand()*1.0)/(RAND_MAX/1.0);
}

void send_file(int sfd, char *line, struct file_s *files, int filecount) {

    // chop off "GET " from the beginning
    line += 4;
    char *filename = line;


    struct file_s *file = NULL;
    for (int i = 0 ; i < filecount ; i++) {
        if (strncmp(filename, files[i].filename, strlen(files[i].filename))==0) {
            file = &files[i];
            break;
        }
    }
    if (file == NULL) {
        printf("file '%s' not found\n", filename);

        write(sfd, no_such_file, strlen(no_such_file));

        return;
    }

    FILE *f = fopen(file->filepath, "rb");
    if (f == NULL) {
        printf("failed to open '%s' for reading\n", file->filepath);
        return;
    }
    write(sfd, "+OK\n", 4);

    long filesize = file->filesize;

    // 30% chance of transmission error
    long transmission_error
        = file->accessed_hash
        ? rand_float() <= 0.3
        : 0;
    // printf("transmission_error? %ld\n", transmission_error);
    // calculate random location in the file for the
    // transmission error to occur
    if (transmission_error) {
        transmission_error = (long)(rand_float()*filesize);
        // printf("transmission_error: %ld\n", transmission_error);
    }
    // turn off transmission error
    else
        transmission_error = -1;

    long remaining = filesize;
    int chunk_size = 1024;

    while (remaining > 0) {
        char buf[chunk_size];
        if (remaining < chunk_size)
            chunk_size = remaining;
        fread(buf, chunk_size, 1, f);

        // simulate transmission error
        if (transmission_error >= 0) {
            // just keep advancing until we get to the transmission error bit
            if (transmission_error > chunk_size)
                transmission_error -= chunk_size;
            // induce the transmission error
            else {
                short bit = (short)(rand_float()*8);
                char mask = (char)0x1<<bit;
                // bit is already a 1, so make it 0
                if (((buf[transmission_error]>>bit)&0x1)==1)
                    buf[transmission_error] ^= mask;
                // bit is a 0, so make it 1
                else
                    buf[transmission_error] |= mask;
                // printf("bit number: %d; original byte: %c; mask: %d; byte after modification: %c\n", bit, byte, mask, buf[transmission_error]);
                transmission_error = -1;
            }
        }

        write(sfd, buf, chunk_size);
        remaining -= chunk_size;
    }
    // write(sfd, "\n", 1);
}

void send_size(int sfd, char *line, struct file_s *files, int filecount) {

    // chop off "SIZE " from the beginning
    line += 5;
    char *filename = line;

    struct file_s *file = NULL;
    for (int i = 0 ; i < filecount ; i++) {
        if (strncmp(filename, files[i].filename, strlen(files[i].filename))==0) {
            file = &files[i];
            break;
        }
    }
    if (file == NULL) {
        printf("file '%s' not found\n", filename);
        write(sfd, no_such_file, strlen(no_such_file));
        return;
    }

    write(sfd, "+OK ", 4);

    long filesize = file->filesize;
    char filesize_str[24];
    sprintf(filesize_str, "%ld\n", filesize);
    write(sfd, filesize_str, strlen(filesize_str));
}

void send_hash(int sfd, char *line, struct file_s *files, int filecount) {
    // chop off "HASH "
    line += 5;

    struct file_s *file = NULL;
    for (int i = 0 ; i < filecount ; i++) {
        if (strncmp(files[i].filename, line, strlen(files[i].filename))==0) {
            file = &files[i];
            break;
        }
    }
    if (file==NULL) {
        printf("couldn't find file '%s' to get a hash\n", line);
        write(sfd, no_such_file, strlen(no_such_file));
        return;
    }

    char *file_md5 = md5_file(file->filepath);

    // "+OK 0b7c616a7d718dc11cddcadbb2649d5b\n"
    write(sfd, "+OK ", 4);
    write(sfd, file_md5, strlen(file_md5));
    write(sfd, "\n", 1);

    file->accessed_hash = 1;
}

void send_hello(int sfd, char *line) {
    char *start_of_name = NULL;
    if (strlen(line)>=6)
        start_of_name = strstr(line, " ");
    char *end_of_name = NULL;
    if (start_of_name != NULL) {
        start_of_name += 1;
        end_of_name = strstr(start_of_name, " ");
    }
    if (end_of_name != NULL)
        end_of_name[0] = '\0';


    char name_section[50];
    if (start_of_name != NULL)
        sprintf(name_section, ", %s", start_of_name);
    else
        name_section[0] = '\0';

    int rnum = (int)(rand_float()*13);
    char *base_string;
    switch (rnum) {
        case 0: base_string = "Aroha%s. Kua hari ki te whakatau ia koe.\n";break;
        case 1: base_string = "Bonjour%s. Ravi de vous rencontrer.\n";break;
        case 2: base_string = "Ciao%s. Piacere di conoscerti.\n";break;
        case 3: base_string = "Hallo%s. Bly te kenne.\n";break;
        case 4: base_string = "Halo%s. Senang bertemu denganmu.\n";break;
        case 5: base_string = "Hello%s. Pleased to meet you.\n";break;
        case 6: base_string = "Hola%s. Encantado de conocerte.\n";break;
        case 7: base_string = "Konnichiwa%s. Aete ureshii yo.\n";break;
        case 8: base_string = "Nyob zoo%s. Txaus siab kom ntsib koj.\n";break;
        case 9: base_string = "Privet%s. Rad vstreche s vami.\n";break;
        case 10:base_string = "Pryvit%s. Pryyemno zustritysya z vamy.\n";break;
        case 11:base_string = "Talofa%s. Fiafia lava e faatasi ma oe.\n";break;
        default:base_string = "Zdravo%s. Drago mi je.\n";break;
    }

    char hello_line[200];
    sprintf(hello_line, base_string, name_section);
    write(sfd, "+OK ", 4);
    write(sfd, hello_line, strlen(hello_line));
}
